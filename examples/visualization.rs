use crate::detail::graph::GraphImpl;
use crate::detail::terrain_generator::{generate_terrain, TerrainDefinition};
use path_finding::a_star;
use std::io;
use std::io::{stdin, Write};

mod detail;

struct RunTime {
    pub size: (i64, i64),
    pub origin: (i64, i64),
    pub goal: (i64, i64),
}

impl RunTime {
    fn new() -> RunTime {
        RunTime {
            size: (0, 0),
            origin: (0, 0),
            goal: (0, 0),
        }
    }
    fn set_size(&mut self, size: (i64, i64)) {
        println!("Setting map size to {}x{}", size.0, size.1);
        self.size = size;
    }
    fn set_from(&mut self, from: (i64, i64)) {
        println!("Setting origin to {}x{}", from.0, from.1);
        self.origin = from;
    }
    fn set_to(&mut self, to: (i64, i64)) {
        println!("Setting goal to {}x{}", to.0, to.1);
        self.goal = to;
    }
}

fn draw_solution(result: &Vec<(i64, i64)>, size: (i64, i64)) {
    for row in 0..=size.0 {
        for column in 0..=size.1 {
            if result.contains(&(row, column)) {
                print!(" 💿 ");
            } else {
                print!(" 🧱 ");
            }
        }
        println!("");
    }
}

fn main() {
    let mut is_running = true;
    let mut input = String::new();
    let mut rt = RunTime::new();

    println!(
        "Commands: \
    [set size] \
    [set origin] \
    [set goal] \
    [size = print size] \
    [origin = print origin] \
    [goal = print goal] \
    [run] \
    [q = quit] \
    "
    );
    while is_running {
        stdin()
            .read_line(&mut input)
            .expect("Did not enter a correct string");
        input = String::from(input.trim());

        match input.as_str() {
            "q" => is_running = false,
            "help" => println!(
                "Commands: \
    [set size] \
    [set origin] \
    [set goal] \
    [size = print size] \
    [origin = print origin] \
    [goal = print goal] \
    [run] \
    [q = quit] \
    "
            ),
            "size" => println!("Size: {:?}", rt.size),
            "set size" => {
                let mut read = String::new();
                print!("Size (with space): ");
                io::stdout().flush().unwrap();
                stdin()
                    .read_line(&mut read)
                    .expect("Did not enter a correct string");
                let mut split = read.trim().split_whitespace();

                rt.set_size((
                    split.next().unwrap().parse().unwrap(),
                    split.next().unwrap().parse().unwrap(),
                ));
            }
            "origin" => println!("Origin: {:?}", rt.origin),
            "set origin" => {
                let mut read = String::new();
                print!("Start (with space): ");
                io::stdout().flush().unwrap();
                stdin()
                    .read_line(&mut read)
                    .expect("Did not enter a correct string");
                let mut split = read.trim().split_whitespace();

                rt.set_from((
                    split.next().unwrap().parse().unwrap(),
                    split.next().unwrap().parse().unwrap(),
                ));
            }
            "goal" => println!("Goal: {:?}", rt.goal),
            "set goal" => {
                let mut read = String::new();
                print!("Goal (with space): ");
                io::stdout().flush().unwrap();
                stdin()
                    .read_line(&mut read)
                    .expect("Did not enter a correct string");
                let mut split = read.trim().split_whitespace();

                rt.set_to((
                    split.next().unwrap().parse().unwrap(),
                    split.next().unwrap().parse().unwrap(),
                ));
            }
            "run" => {
                println!(
                    "Running size: {}x{}, from {}x{} to {}x{}",
                    rt.size.0, rt.size.1, rt.origin.0, rt.origin.1, rt.goal.0, rt.goal.1
                );

                let graph = GraphImpl::new(generate_terrain(TerrainDefinition {
                    size_x: rt.size.0,
                    size_y: rt.size.1,
                    size_z: None,
                }));
                let result = a_star::get_path(rt.origin, rt.goal, &graph);

                println!("Result: {:?};", result);
                println!("");
                draw_solution(&result, rt.size);
            }
            _ => println!("Incorrect command."),
        }

        input = String::new();
    }
}

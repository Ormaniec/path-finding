use std::fmt::Formatter;

pub struct Terrain {
    pub terrain: Vec<Vec<u64>>,
}

impl std::fmt::Display for Terrain {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut map: String = String::new();

        map += "[ ";

        for row in &self.terrain {
            for value in row {
                map += (value.to_string() + ", ").as_str();
            }
            map += "\n  ";
        }

        map.pop();
        map.pop();
        map.pop();
        map.pop();
        map.pop();
        map += " ]";

        write!(f, "{}", map)
    }
}

use crate::detail::terrain::Terrain;

pub struct TerrainDefinition {
    pub size_x: i64,
    pub size_y: i64,
    pub size_z: Option<i64>,
}

pub fn generate_terrain(definition: TerrainDefinition) -> Terrain {
    match definition.size_z {
        None => generate_2d_terrain(definition),
        Some(_) => generate_3d_terrain(definition),
    }
}

// FIXME: Currently we generate only empty board, which is not functional
pub fn generate_2d_terrain(definition: TerrainDefinition) -> Terrain {
    Terrain {
        terrain: vec![vec![0; definition.size_y as usize]; definition.size_x as usize],
    }
}

pub fn generate_3d_terrain(_: TerrainDefinition) -> Terrain {
    panic!("3D is not yet supported")
}

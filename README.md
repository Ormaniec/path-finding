# A* path finding solution for rust

Project is meant to deliver generic solution easily importable into code of any
developer as crate. It will deliver simple interface and examples of usage for
any developer interested in utilizing this library. Most important in this
implementation should be performance and thread separation, to let this
solution be used for however many agents it's required.

# Functionalities:

- Example visualization
- Simple generic interface

# Usage:

Include dependency:

```toml
[dependencies]
path_finding = { git = "https://gitlab.com/Ormaniec/path-finding.git" }
```

Implement methods (Follow documentation of `a_star::get_path`):

```rust
    use path_finding::a_star;

    fn main() {
        a_star::get_path(/*Your input*/);
    }
```
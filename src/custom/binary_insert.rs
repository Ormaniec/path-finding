use std::collections::HashMap;

pub fn binary_insert<N>(node: &N, insert_target: &mut Vec<N>, distance_map: &HashMap<N, f64>)
where
    N: PartialEq + Eq + std::hash::Hash + Clone,
{
    if insert_target.is_empty() {
        return insert_target.push(node.clone());
    }

    let distance_from_node = distance_map[node];

    let mut low = 0;
    let mut high = insert_target.len() as i32 - 1;

    let mut mid;
    let mut mid_index = 0i32;

    while low <= high {
        mid = ((high - low) / 2) + low;
        mid_index = mid;

        let current = &insert_target[mid_index as usize];
        let distance_from_current = distance_map[current];

        if distance_from_node == distance_from_current {
            break;
        } else if distance_from_node < distance_from_current {
            low = mid + 1;
        } else if distance_from_node > distance_from_current {
            high = mid - 1;
        }
    }

    insert_target.insert(mid_index as usize + 1, node.clone());

    let mut previous = -1i32;

    for i in 0..insert_target.len() {
        if insert_target[i] == *node {
            if previous != -1 {
                insert_target.remove(previous as usize);
            }
            previous = i as i32;
        }
    }
}

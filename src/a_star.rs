use crate::custom::binary_insert::binary_insert;
use std::collections::HashMap;
use std::hash::Hash;

/// # Graph
/// Graph represents your non-generic structure which you can input into A*
/// algorithm. This graph contains simple methods, which have to be implemented
/// by yourself, to correctly represent your structure.
///
/// # Using with already existing structures
/// To use with already existing map, you can just implement Graph based
/// on your node type, annotated here with name of`N`.
pub trait Graph<N> {
    /// # Heuristic
    /// Implement here your own method to calculate distance from source
    /// to target. This allows you to create own constraints.
    fn get_distance_to_target(&self, start: &N, target: &N) -> f64;

    /// # Pathing
    /// Implement here your method to know which path is not blocked by
    /// existing structure. If it's blocked, it will simply not be added
    /// to `open_set` of paths.
    fn get_open_neighbour_node_vec(&self, node: &N) -> Vec<N>;

    /// # Disturbances
    /// Implement here detection of any potential disturbances.
    ///
    /// # Example
    /// If your character is faster on water, return here lower value,
    /// if it's slower return larger value.
    fn get_edge_weight(&self, from: &N, to: &N) -> f64;
}

/// # A* get_path
/// This method will take your structures and use your implementation
/// of `Graph` to find optimal path from point `start` to point
/// `target`.
///
/// # Example
///
/// ```rust
///     use path_finding::a_star;
///     use path_finding::a_star::Graph;
///
///     struct YourMap;
///     impl Graph<N> for YourMap{fn get_distance_to_target(&self, start: &N, target: &N) -> f64 {
///         todo!()
///     }
///
///     fn get_open_neighbour_node_vec(&self, node: &N) -> Vec<N> {
///         todo!()
///     }
///
///     fn get_edge_weight(&self, from: &N, to: &N) -> f64 {
///         todo!()
///     }}
///
///     let graph = YourMap::new();
///     let origin;
///     let goal;
///
///     let result = a_star::get_path(origin, goal, &graph);
/// ```
///
/// # **NOTE**
/// You have to implement graph methods into your map instance!
pub fn get_path<N>(start: N, target: N, graph: &impl Graph<N>) -> Vec<N>
    where
        N: Eq + Copy,
        N: Hash,
{
    let mut open_set = Vec::new();
    let mut came_from = HashMap::new();
    let mut g_score = HashMap::new();
    let mut f_score = HashMap::new();

    open_set.push(start.clone());

    g_score.insert(start.clone(), 0.0);
    f_score.insert(start.clone(), graph.get_distance_to_target(&start, &target));

    while !open_set.is_empty() {
        let current = open_set.pop().unwrap();

        if current == target {
            return reconstruct(&came_from, &target);
        }

        for neighbour in graph.get_open_neighbour_node_vec(&current) {
            let tentative_g_score = g_score[&current] + graph.get_edge_weight(&current, &neighbour);
            if tentative_g_score < *g_score.entry(neighbour).or_insert(f64::MAX) {
                came_from
                    .entry(neighbour.clone())
                    .or_insert(current.clone());
                g_score.insert(
                    neighbour.clone(),
                    graph.get_distance_to_target(&neighbour, &start),
                );
                f_score.insert(
                    neighbour.clone(),
                    graph.get_distance_to_target(&neighbour, &target),
                );

                if !open_set.contains(&&neighbour) {
                    binary_insert(&neighbour, &mut open_set, &g_score);
                }
            }
        }
    }

    Vec::new()
}

fn reconstruct<N>(came_from: &HashMap<N, N>, goal: &N) -> Vec<N>
    where
        N: Eq + Copy,
        N: Hash,
{
    let mut current = goal.clone();
    let mut total_path = vec![current.clone()];
    while came_from.contains_key(&current) {
        current = came_from[&current];
        total_path.push(current);
    }

    total_path
}

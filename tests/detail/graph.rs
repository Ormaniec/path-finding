use crate::detail::terrain::Terrain;
use path_finding::a_star::Graph;

pub struct GraphImpl {
    pub terrain: Terrain,
}

impl GraphImpl {
    pub fn new(terrain: Terrain) -> GraphImpl {
        GraphImpl { terrain }
    }
}

impl Graph<(i32, i32)> for GraphImpl {
    fn get_distance_to_target(&self, current: &(i32, i32), target: &(i32, i32)) -> f64 {
        let distance_x = current.0 as f64 - target.0 as f64;
        let distance_y = current.1 as f64 - target.1 as f64;

        (distance_x.powi(2) + distance_y.powi(2)).sqrt()
    }

    fn get_open_neighbour_node_vec(&self, node: &(i32, i32)) -> Vec<(i32, i32)> {
        vec![
            (node.0 - 1, node.1 - 1),
            (node.0 - 1, node.1),
            (node.0 - 1, node.1 + 1),
            (node.0, node.1 - 1),
            (node.0, node.1 + 1),
            (node.0 + 1, node.1 - 1),
            (node.0 + 1, node.1),
            (node.0 + 1, node.1 + 1),
        ]
    }

    fn get_edge_weight(&self, _: &(i32, i32), _: &(i32, i32)) -> f64 {
        0.0
    }
}

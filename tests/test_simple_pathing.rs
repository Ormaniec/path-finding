mod detail;

#[cfg(test)]
mod tests {
    use crate::detail::graph::GraphImpl;
    use crate::detail::terrain_generator::{generate_terrain, TerrainDefinition};
    use path_finding::a_star::get_path;

    #[test]
    fn test_corner_to_corner() {
        let graph = GraphImpl::new(generate_terrain(TerrainDefinition {
            size_x: 10,
            size_y: 10,
            size_z: None,
        }));

        let result = get_path((0, 0), (10, 10), &graph);
        let solution = vec![
            (10, 10),
            (9, 9),
            (8, 8),
            (7, 7),
            (6, 6),
            (5, 5),
            (4, 4),
            (3, 3),
            (2, 2),
            (1, 1),
            (0, 0),
        ];

        assert_eq!(result, solution);
    }
    #[test]
    fn test_corner_to_middle() {
        let graph = GraphImpl::new(generate_terrain(TerrainDefinition {
            size_x: 10,
            size_y: 10,
            size_z: None,
        }));

        let result = get_path((0, 0), (5, 5), &graph);
        let solution = vec![(5, 5), (4, 4), (3, 3), (2, 2), (1, 1), (0, 0)];

        assert_eq!(result, solution);
    }

    #[test]
    fn test_corner_to_3_7() {
        let graph = GraphImpl::new(generate_terrain(TerrainDefinition {
            size_x: 10,
            size_y: 10,
            size_z: None,
        }));

        let result = get_path((0, 0), (3, 7), &graph);
        let solution = vec![
            (3, 7),
            (2, 6),
            (1, 5),
            (0, 4),
            (0, 3),
            (0, 2),
            (0, 1),
            (0, 0),
        ];

        assert_eq!(result, solution);
    }

    #[test]
    fn test_corner_to_5_9() {
        let graph = GraphImpl::new(generate_terrain(TerrainDefinition {
            size_x: 10,
            size_y: 10,
            size_z: None,
        }));

        let result = get_path((0, 0), (5, 9), &graph);
        let solution = vec![
            (5, 9),
            (4, 8),
            (3, 7),
            (2, 6),
            (1, 5),
            (0, 4),
            (0, 3),
            (0, 2),
            (0, 1),
            (0, 0),
        ];

        assert_eq!(result, solution);
    }

    #[test]
    fn test_4_5_to_5_9() {
        let graph = GraphImpl::new(generate_terrain(TerrainDefinition {
            size_x: 10,
            size_y: 10,
            size_z: None,
        }));

        let result = get_path((4, 5), (5, 9), &graph);
        let solution = vec![(5, 9), (4, 8), (4, 7), (4, 6), (4, 5)];

        assert_eq!(result, solution);
    }

    #[test]
    fn test_10_10_to_5_9() {
        let graph = GraphImpl::new(generate_terrain(TerrainDefinition {
            size_x: 10,
            size_y: 10,
            size_z: None,
        }));

        let result = get_path((10, 10), (9, 5), &graph);
        let solution = vec![(9, 5), (10, 6), (10, 7), (10, 8), (10, 9), (10, 10)];

        assert_eq!(result, solution);
    }
}
